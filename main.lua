-- bootstrap the compiler
local fennel_module = "lib.fennel"
fennel = require (fennel_module)
table.insert(package.loaders, fennel.make_searcher({correlate = true,
                                                    moduleName = fennel_module,
                                                    useMetadata = true,}))

package.loaded.fennel = fennel

fennel.path = love.filesystem.getSource() .. "/?.fnl;" ..
   love.filesystem.getSource() .. "/src/?.fnl;" ..
   fennel.path

-- package.path = "./src/?.lua;".. package.path

pp = function(x) print(require("lib.fennelview")(x)) end
lume = require("lib.lume")

_Gdata = {}
require("wrap")
