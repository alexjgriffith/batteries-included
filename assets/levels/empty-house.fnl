{
  :data {
    :clouds {}
    :for1 {
      7692 {
        :h 8
        :id 0
        :l "for1"
        :library "tile"
        :type "stairs"
        :w 4
        :x 12
        :y 2
      }                
      19204 {
        :h 8
        :id 0
        :l "for1"
        :library "tile"
        :type "stairs-flip"
        :w 4
        :x 4
        :y 5
      }
      23047 {
        :h 6
        :id 0
        :l "for1"
        :library "tile"
        :type "door"
        :w 4
        :x 7
        :y 6
      }
    }
    :for2 {}
    :ground {
      0 {
        :id 0
        :index [ 5 8 6 1 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 0
      }
      3840 {
        :id 0
        :index [ 6 12 6 12 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 1
      }
      3854 {
        :id 0
        :index [ 6 12 6 12 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 1
      }
      7680 {
        :id 0
        :index [ 6 12 6 12 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 2
      }
      7694 {
        :id 0
        :index [ 6 12 6 12 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 2
      }
      11520 {
        :id 0
        :index [ 6 2 6 1 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 3
      }
      11521 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 3
      }
      11522 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 2
        :y 3
      }
      11523 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 3
        :y 3
      }
      11524 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 4
        :y 3
      }
      11525 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 3
      }
      11526 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 3
      }
      11527 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 3
      }
      11528 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 3
      }
      11529 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 3
      }
      11530 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 3
      }
      11531 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 11
        :y 3
      }
      11532 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 3
      }
      11533 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 13
        :y 3
      }
      11534 {
        :id 0
        :index [ 4 12 3 12 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 3
      }
      15360 {
        :id 0
        :index [ 6 12 6 12 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 4
      }
      15374 {
        :id 0
        :index [ 6 12 6 12 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 4
      }
      19200 {
        :id 0
        :index [ 6 12 6 12 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 5
      }
      19214 {
        :id 0
        :index [ 6 12 6 12 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 5
      }
      23040 {
        :id 0
        :index [ 6 2 6 1 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 6
      }
      23041 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 6
      }
      23042 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 2
        :y 6
      }
      23043 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 3
        :y 6
      }
      23044 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 4
        :y 6
      }
      23045 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 6
      }
      23046 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 6
      }
      23047 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 6
      }
      23048 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 6
      }
      23049 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 6
      }
      23050 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 6
      }
      23051 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 11
        :y 6
      }
      23052 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 6
      }
      23053 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 13
        :y 6
      }
      23054 {
        :id 0
        :index [ 4 12 3 12 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 6
      }
      26880 {
        :id 0
        :index [ 6 12 6 12 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 7
      }
      26894 {
        :id 0
        :index [ 6 12 6 12 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 7
      }
      30720 {
        :id 0
        :index [ 6 12 6 12 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 8
      }
      30734 {
        :id 0
        :index [ 6 12 6 12 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 8
      }
      34560 {
        :id 0
        :index [ 6 2 7 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 0
        :y 9
      }
      34561 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 9
      }
      34562 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 2
        :y 9
      }
      34563 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 3
        :y 9
      }
      34564 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 4
        :y 9
      }
      34565 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 9
      }
      34566 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 9
      }
      34567 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 9
      }
      34568 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 9
      }
      34569 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 9
      }
      34570 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 9
      }
      34571 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 11
        :y 9
      }
      34572 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 9
      }
      34573 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 13
        :y 9
      }
      34574 {
        :id 0
        :index [ 4 12 10 13 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 9
      }
      1 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 1
        :y 0
      }
      2 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 2
        :y 0
      }
      3 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 3
        :y 0
      }
      4 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 4
        :y 0
      }
      5 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 5
        :y 0
      }
      6 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 6
        :y 0
      }
      7 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 7
        :y 0
      }
      8 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 8
        :y 0
      }
      9 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 9
        :y 0
      }
      10 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 10
        :y 0
      }
      11 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 11
        :y 0
      }
      12 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 12
        :y 0
      }
      13 {
        :id 0
        :index [ 8 8 10 10 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 13
        :y 0
      }
      14 {
        :id 0
        :index [ 8 11 3 12 ]
        :l "ground"
        :library "subtile"
        :type "floor"
        :x 14
        :y 0
      }
    }
    :objs {
           7693 {
        :activities [ {
            :activity "tello-down-2"
            :base 10
            :bias 1
            :stat "special"
          } ]
                 
        :h 2
        :id 0
        :l "objs"
        :library "ntile"
        :type "darrow"
        :w 2
        :x 13
        :y 2
      }
      19204 {
        :activities [ {
            :activity "tello-down-1"
            :base 10
            :bias 1
            :stat "special"
          } ]
        :h 2
        :health 3
        :id 0
        :l "objs"
        :library "ntile"
        :type "darrow"
        :w 2
        :x 4
        :y 5
      }

           19212 {
        :activities [ {
            :activity "tello-up-2"
            :base 10
            :bias 1
            :stat "special"
          } ]
                  
        :h 2
        :id 0
        :l "objs"
        :library "ntile"
        :type "uarrow"
        :w 2
        :x 12
        :y 5
      }
      26893 {
        :activities [ {
            :activity "claw"
            :base 10
            :bias 1
            :stat "boredom"
          } ]
        :h 4
        :health 1
        :id 0
        :l "objs"
        :library "tile"
        :type "fridge"
        :w 2
        :x 13
        :y 7
      }
      30725 {
        :activities [ {
            :activity "tello-up-1"
            :base 10
            :bias 1
            :stat "special"
          } ]
        :h 2
        :health 3
        :id 0
        :l "objs"
        :library "ntile"
        :type "uarrow"
        :w 2
        :x 5
        :y 8
      }
      30730 {
        :activities [ {
            :activity "eat"
            :base 10
            :bias 1
            :stat "hunger"
          } {
            :activity "meaw"
            :base 1
            :bias 1
            :stat "hunger"
          } ]
        :h 2
        :health -8
        :id 0
        :l "objs"
        :library "tile"
        :type "food"
        :w 2
        :x 10
        :y 8
      }
      30732 {
        :activities [ {
            :activity "sleep"
            :base 10
            :bias 1
            :stat "sleepy"
          } ]
        :h 2
        :health 3
        :id 0
        :l "objs"
        :library "tile"
        :type "oven"
        :w 2
        :x 12
        :y 8
      }
    }
    :sun {}
  }
  :height 2560
  :id 0
  :tilesize 2
  :width 3840
}
