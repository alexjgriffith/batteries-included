 local colour = {_version = "0.1.0"}

























 local math_max = math.max
 local math_min = math.min
 local math_floor = math.floor
 local math_exp = math.exp
 local table_insert = table.insert
 local table_concat = table.concat

 local string_char = string.char


 local function map(t, fun) local rtn = {}


 for key, value in pairs(t) do
 rtn[key] = fun(value) end
 return rtn end

 local function max(t)

 local ret = t[1]
 for i = 2, #t do
 local v = t[i] if (v > ret) then

 ret = v end end
 return ret end

 local function min(t)

 local ret = t[1]
 for i = 2, #t do
 local v = t[i] if (v < ret) then

 ret = v end end
 return ret end

 local function int_clamp_0_255(value)

 return math_min(math_max(math_floor((255 * value)), 0), 255) end

 local function hex_to_dec(hex)

 local first = string.sub(hex, 1, 1)
 local second = string.sub(hex, 2, 2) local to_dec = {A = 10, B = 11, C = 12, D = 13, E = 14, F = 15, ["0"] = 0, ["1"] = 1, ["2"] = 2, ["3"] = 3, ["4"] = 4, ["5"] = 5, ["6"] = 6, ["7"] = 7, ["8"] = 8, ["9"] = 9, a = 10, b = 11, c = 12, d = 13, e = 14, f = 15}










 return ((16 * to_dec[first]) + to_dec[second]) end


 colour["hex-to-rgba"] = function(hex) local hexcodes = {string.sub(hex, 2, 3), string.sub(hex, 4, 5), string.sub(hex, 6, 7), "FF"}











 local function _0_(x) return (x / 255) end return map(map(hexcodes, hex_to_dec), _0_) end




 colour["hsl-to-rgb"] = function(hin, s, l, a) local _0_ = {0, 0, 0}


 local r = _0_[1] local g = _0_[2] local b = _0_[3]
 local h = (hin / 360)
 local hue2rgb = nil local function hue2rgb0(p, q, tin)
 local t = tin if (t < 0) then
 t = (t + 1) end if (t > 1) then
 t = (t - 1) end
 if (t < (1 / 6)) then
 return (p + (t * 6 * (q + ( - p)))) elseif (t < (1 / 2)) then
 return q elseif (t < (2 / 3)) then
 return (p + (((2 / 3) + ( - t)) * 6 * (q + ( - p)))) else
 return p end end hue2rgb = hue2rgb0
 if (s == 0) then local _1_ = {l, l, l}
 r = _1_[1] g = _1_[2] b = _1_[3] else
 local q = nil if (l < 0.5) then q = (l * (1 + s)) else q = (l + s + ( - (l * s))) end
 local p = ((2 * l) + ( - q))
 r = hue2rgb(p, q, (h + (1 / 3)))
 g = hue2rgb(p, q, h)
 b = hue2rgb(p, q, (h + ( - (1 / 3)))) end return {r, g, b, a} end


 colour.normalize = function(te, min_3f, max_3f)



 local max0 = (max_3f or max(te))
 local min0 = (min_3f or min(te))
 local function _0_(e) return ((e + ( - min0)) / (max0 + ( - min0))) end return map(te, _0_) end

 colour.sigmoid = function(x, vmax, xmid, k)


 return (vmax / (1 + math_exp(( - (k * (x + ( - xmid))))))) end

 colour.noise = function(noise, width, height, off, freq, amp) local value = {} local val = 0




 for i = 1, width do
 for j = 1, height do val = 0

 for k = 1, 6 do
 val = (val + ((amp / k) * noise((off + (freq * k * (i / width))), (off + (freq * k * (j / height)))))) end


 table_insert(value, val) end end
 return colour.normalize(value) end

 colour["rgb-to-byte"] = function(_0_0) local _1_ = _0_0 local r = _1_[1] local g = _1_[2] local b = _1_[3] local a = _1_[4]

 return string_char(int_clamp_0_255(r), int_clamp_0_255(g), int_clamp_0_255(b), int_clamp_0_255(a)) end


 colour["rgb-array-to-bytedata"] = function(array)

 local rgb_to_byte = colour["rgb-to-byte"] local tab = {}

 for _, rgb in ipairs(array) do
 table_insert(tab, rgb_to_byte(rgb)) end
 return table_concat(tab, "") end

 colour["hsl-array-to-bytedata"] = function(array)

 local rgb_to_byte = colour["rgb-to-byte"]
 local hsl_to_rgb = colour["hsl-to-rgb"] local tab = {}

 for _, rgb in ipairs(array) do

 table_insert(tab, rgb_to_byte(hsl_to_rgb(unpack(rgb)))) end
 return table_concat(tab, "") end

 return colour
