(local text-box {})

;; how do we integrate text highlighting into this?
;; is it possible to make selectable text?
;; break on new lines
;; each section will have a font
;; each section will have a colour
;; each section will have a weight
;; each section will have alignment?
;; each section will have a size
;; (local styles {:default {:font x :colour black :weight :normal :size 16 :inherits null
;                 :player-name {:inherits :default :weight :bold}}})
;; {:introduction
;;    [default
;;      "this is the default"
;;      [player-name "this is some more text"]
;;      "some more default"]}

;; How do we handle kerning?
;; [keyname, font, colour, weight, size, x, y w, string, written]

;; [line-1 [params {}{}] ... line-n[params {}{}]]
;; [[[style text width][style text width]] ... line-n[params {}{}]]

(fn text-box.text-to-box [font str width height]
  (let [font-height (font:getHeight)
        [wrapped-string lines] (text-box.wrap-para font str width)
        box-height (* font-height lines)]
    {:wrapped wrapped-string
     :original str
     :width width
     :visible-height height
     :box-height box-height
     :font-height font-height
     :scroll 0
     :written ""
     :index 1}
    )
  )

(fn text-box.wrap-para [font str width]
    (->
     str
     (lume.split " ")
     (lume.map  (fn [word] [(font:getWidth word) word]))
     (lume.reduce
      (fn [[accu count lines] [len str]]
        (if (< (+ count len) width)
            [(.. accu " " str) (+ count len) lines]
            [(.. accu "\n" str) 0 (+ lines 1)]))
      ["" 0 1])
     ((fn [[str _ lines]] [(string.sub str 2) lines]))))

(fn text-box.wrap-string [font str width]
  ;; Font:getWrap( text, wraplimit )
  (->
   str
   (lume.split "\n")
   (lume.reduce
    (fn [[str count] substring]
      (let [[new-str lines] (text-box.wrap-para font substring width)]
        [(.. str "\n\n" new-str) (+ count lines)])
      )
    ["" 0]
    )
   ((fn [[str count]] [(string.sub str 3) count]))))

(fn text-box.wrap-string-example []
  (text-box.text-to-box (assets.fonts.inconsolata 16) "The loaders option specifies how to load assets. Cargo uses filename extensions to determine how to load files. The keys of entries in the loaders table are the file extensions. These map to functions that take in a filename and return a loaded asset. In the above example, we run the function love.graphics.newImage on any filenames that end in .jpg" 200 100))

(tset _G :tb text-box)

text-box
