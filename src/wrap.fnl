;; move to lua?
(local gamestate (require :lib.gamestate))
(local repl (require :lib.stdio))
(local cargo (require :lib.cargo))

(math.randomseed (os.time))

(fn love.load []
  (love.filesystem.setIdentity "screenshot_example")
  ;; load all assets
  (tset _G :assets (cargo.init :assets))
  (gamestate.registerEvents)
  ;; enter the first game state
  (gamestate.switch (require "game-state"))
  (repl.start))
