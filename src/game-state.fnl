(local example  {})

(fn draw-background [w h]
  (love.graphics.push "all")
  (love.graphics.setColor 1 1 1 1 )
  (love.graphics.rectangle "fill" 0 0 w h)
  (love.graphics.pop))

(fn draw-text [text x y]
  (love.graphics.push "all")
  (love.graphics.setFont (assets.fonts.inconsolata 16))
  (love.graphics.setColor 0 0 0 1 )
  (love.graphics.print text x y)
  (love.graphics.pop))

(fn example.draw []
  (let [(window-width window-height  _) (love.window.getMode)]
    (draw-background window-width window-height)
    (draw-text "Hello World 😊!\nHello again!" 20 20)))

(var gifs {})

(fn start-gif [name time?]
  (local max-frames 1000)
  (fn update [self dt]
    (set self.dt (+ dt self.dt))
    (when (> self.dt self.time)
      (do
          (set self.frame (+ self.frame 1))
                 (set self.dt (- self.dt self.time ))
                 (love.graphics.captureScreenshot (string.format "recording/%012d.png" self.frame))))
    (when (> self.frame max-frames)
      (self:end)))
  (fn end [self]
    (set self.active false)
    (os.execute (.. "ffmpeg -i "  (love.filesystem.getSaveDirectory) "/recording/%012d.png " (love.filesystem.getSaveDirectory) self.name  ".gif"))
    (os.execute (.. "rm "  (love.filesystem.getSaveDirectory)"/recording/*.png")))
  (set gifs {
             :active true
             :frame 0
             :time (or time? 0.1)
             :dt 0
             :name name
             :update update
             :end end
             })
  )


(fn update-gif [dt]
  (when (and gifs gifs.active (= gifs.active true))
    (gifs:update dt)
  ))


(fn example.update [self dt]
  (update-gif dt)
)

(fn example.keyreleased [_ code]
  (match code
    "c"   (start-gif "sample")
    "s"   (gifs:end)
    "escape" (love.event.quit)
    "return" (let [(w h opts) (love.window.getMode)]
                 (if opts.fullscreen
                     (love.window.setFullscreen false)
                     (love.window.setFullscreen true "desktop")))))


example
